﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
               
            Console.Clear();
             
             var month = "";

            Console.WriteLine("What month were you born in?");
           
            month = Console.ReadLine();

            Console.WriteLine($"You were born in {month}");



         
             Console.ResetColor();
             Console.WriteLine();
             Console.WriteLine("Press <Enter> to quit the program");
             Console.ReadKey();
        }
    }
}
